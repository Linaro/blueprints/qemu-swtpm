FROM debian:sid-slim

COPY install.sh /build/
COPY qemu-swtpm /build/

RUN set -e ;\
    apt update -q=2 ;\
    apt full-upgrade -q=2 --yes ;\
    apt install -q=2 --yes --no-install-recommends fdisk gdisk libguestfs-tools qemu-system qemu-system-gui qemu-utils swtpm swtpm-tools ;\
    cd /build/ && ./install.sh ;\
    # Cleanup
    apt clean ;\
    rm -rf /var/lib/apt/lists/* /var/tmp/* /build/
