# qemu-swtpm - QEMU wrapper for starting VMs with an attached TPM emulator

qemu-swtpm provides wrapper binaries called `qemu-system-*-swtpm` that will
start a swtpm instance and call the corresponding `qemu-system-*` binary with
all your arguments plus the right ones to make the emulated TPM available to
the virtual machine.

## Documentation

See [the manual page](qemu-swtpm.pod).

## License

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
