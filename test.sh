#!/bin/bash

set -xeuo pipefail

ARCH=aarch64
IMAGES_DIR=tmp
DOCKER_IMAGE_NAME=qemu-swtpm-test-image
DOCKER_CONTAINER_NAME=qemu-swtpm-test-container
BOOT_CHECK_STRING="^trs-qemuarm64 login:"

OS=${OS:-'trs-image-trs-qemuarm64.rootfs.wic'}
OS_FILE=${OS_FILE:-'trs-image-trs-qemuarm64.rootfs.wic'}
FIRMWARE=${FIRMWARE:-'flash.bin-qemu'}
FIRMWARE_FILE=${FIRMWARE_FILE:-'flash.bin-qemu'}

OS_URL=${OS_URL:-"https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/${OS_FILE}.bz2?job=build-meta-trs"}
FIRMWARE_URL=${FIRMWARE_URL:-"https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/${FIRMWARE_FILE}.gz?job=build-meta-ts-qemuarm64-secureboot"}

trap cleanup_exit INT TERM EXIT

cleanup_exit()
{
	docker stop ${DOCKER_CONTAINER_NAME} || true
}

mkdir -p "${IMAGES_DIR}"

# download if no file found:
if [ ! -f "${IMAGES_DIR}/${OS_FILE}" ]; then
	if [ -f "${OS}" ]; then
		cp "${OS}" "${IMAGES_DIR}/${OS_FILE}"
	else
		wget "${OS_URL}" -O - | bunzip2 > "${IMAGES_DIR}/${OS_FILE}"
	fi
fi

if [ ! -f "${IMAGES_DIR}/${FIRMWARE_FILE}" ]; then
	if [ -f "${FIRMWARE}" ]; then
		cp "${FIRMWARE}" "${IMAGES_DIR}/${FIRMWARE_FILE}"
	else
		wget "${FIRMWARE_URL}" -O - | gunzip > "${IMAGES_DIR}/${FIRMWARE_FILE}"
	fi
fi

docker build -t "${DOCKER_IMAGE_NAME}" .
(docker \
	run \
	--network=host \
	--cap-add=NET_ADMIN \
	--interactive \
	--rm \
	--init \
	--name="${DOCKER_CONTAINER_NAME}" \
	"--mount=type=bind,source=${PWD}/${IMAGES_DIR},destination=/tmp/images" \
	--env HOME="/tmp/images" \
	--user "$(id -u):$(id -g)" \
	"${DOCKER_IMAGE_NAME}:latest" \
		"/usr/local/bin/qemu-system-${ARCH}-swtpm" \
		-cpu cortex-a57 \
		-machine virt,secure=on \
		-nographic \
		-net nic,model=virtio,macaddr=DE:AD:BE:EF:36:02 \
		-net user \
		-monitor none \
		-drive "id=disk1,file=/tmp/images/${OS_FILE},if=none,format=raw" \
		-device virtio-blk-device,drive=disk1 \
		-nographic \
		-device i6300esb,id=watchdog0 \
		-m 2048 \
		-smp 4 \
		-drive "if=pflash,unit=0,readonly=off,file=/tmp/images/${FIRMWARE_FILE},format=raw"
) 2>&1 > "${IMAGES_DIR}/log" &

# Wait for log file to be created
while true; do
	if test -f "${IMAGES_DIR}/log"; then
		break
	fi
	sleep 1
done

# Wait until it boots
echo "Waiting up to 20 min to boot"
count=0
tail -f "${IMAGES_DIR}/log" &

while [ "$count" -lt 40 ]; do
	echo "count = $count"
	grep "${BOOT_CHECK_STRING}" "${IMAGES_DIR}/log" && break
	sleep 30
	count="$(( $count + 1 ))"
done
[ "$count" -gt 40 ] && ( echo "Timed out"; exit 1 )
echo "Test passed"
exit 0
